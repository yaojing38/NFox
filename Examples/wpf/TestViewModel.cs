﻿using System;
using System.Windows;
using System.Windows.Input;

using NFox.WPF;


namespace wpf
{
    public class TestViewModel : ViewModelBase //继承viewmodel基类
    {
        private bool _IsReceiveMouseMove; //定义一个判断是否接受鼠标移动的属性
        public bool IsReceiveMouseMove
        {
            get { return _IsReceiveMouseMove; }
            set
            {
                Set(ref _IsReceiveMouseMove, value); //调用set函数设置属性的值，注意第一个参数要 ref
            }
        }

        private string _tipText; //定义一个显示文本的属性
        public string TipText
        {
            get { return _tipText; }
            set
            {
                Set(ref _tipText, value); //调用set函数设置属性的值，注意第一个参数要 ref
            }
        }



        private RelayCommand loadedCommand; //定义一个加载命令

        public RelayCommand LoadedCommand
        {
            get
            {
                if (loadedCommand == null)
                {
                    //加载命令的执行内容
                    loadedCommand = new RelayCommand(execute => MessageBox.Show("程序加载完毕！")); 

                }
                return loadedCommand;
            }

        }

        private RelayCommand<MouseEventArgs> mouseMoveCommand; //定义一个鼠标移动命令，注意这是一个鼠标事件
        public RelayCommand<MouseEventArgs> MouseMoveCommand
        {
            get
            {
                if (mouseMoveCommand == null)
                {
                    //鼠标移动事件命令的执行内容
                    mouseMoveCommand =
                      new RelayCommand<MouseEventArgs>(
                            e => //action委托，具体的命令内容
                            {
                                var point = e.GetPosition(e.Device.Target);
                                var left = "左键放开";
                                var mid = "中键放开";
                                var right = "右键放开";

                                if (e.LeftButton == MouseButtonState.Pressed)
                                {
                                    left = "左键放下";
                                }
                                if (e.MiddleButton == MouseButtonState.Pressed)
                                {
                                    mid = "中键放下";
                                }
                                if (e.RightButton == MouseButtonState.Pressed)
                                {
                                    right = "右键放下";
                                }

                                TipText = $"当前鼠标位置 X:{point.X} Y:{point.Y} 当前鼠标状态：{left} {mid} {right}.";
                            },
                        o => IsReceiveMouseMove); //func委托，返回命令是否可用
                }
                return mouseMoveCommand;
            }
        }
    }
}
