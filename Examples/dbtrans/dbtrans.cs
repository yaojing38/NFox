﻿using System;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Internal;



using NFox.Cad;
using NFox.Basal;
namespace dbtrans
{
    public class Dbtrans
    {
        [CommandMethod("test1")]
        public void Test1()
        {
            using var tr = new DBTransaction(); //打开事务管理器，采用c#9.0语法
            var line = new Line(new Point3d(1, 1, 0), new Point3d(2, 2, 0));
            tr.AddEntity(line); //在当前绘图空间添加直线
        }
    }
}
