﻿using System.Xml.Linq;
using ComTypes = System.Runtime.InteropServices.ComTypes;

namespace NFox.Runtime.Com.Reflection
{
    /// <summary>
    /// 字段信息,不全...
    /// </summary>
    public class Field : MemberBase
    {
        internal Field(XElement xe) 
            : base(xe)
        { }

        public Field(int id, string name, ComTypes.ITypeInfo info, ComTypes.VARDESC desc) 
            : base(id, name)
        {
            Element = new Element(info, desc.elemdescVar);
        }

        protected override string Category
        {
            get { return "Field"; }
        }
    }
}